# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = '3fa4191fd78b6eeb4da8b2725b3af58e24114cbcd035e386be3a23ca3b966ba07013d286b79b9c803dc4c77b03cb7ed05794f83eadf2987187cf5289c2d16b4b'
